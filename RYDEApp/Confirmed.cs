﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RYDEApp
{
    public partial class Confirmed : Form
    {
        public Confirmed(string from, string toInput, double bookingFee, double serFee, double disChar, double total, string rydeType)
        {
            InitializeComponent();
            getFrom.Text = from;
            getTo.Text = toInput;
            getBooFee.Text = "$" + bookingFee.ToString();
            getPerKmFee.Text = "$" + disChar.ToString();
            getServiceFee.Text = "$" + serFee.ToString();
            getTotal.Text = "$" + total.ToString();
            PoolType.Text = rydeType;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            
        }

        private void getTo_Click(object sender, EventArgs e)
        {
           
        }
    }
}
