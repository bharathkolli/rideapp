﻿namespace RYDEApp
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.FindRide = new System.Windows.Forms.Button();
            this.RydePool = new System.Windows.Forms.RadioButton();
            this.RydeDirect = new System.Windows.Forms.RadioButton();
            this.FromLabel = new System.Windows.Forms.Label();
            this.ToLabel = new System.Windows.Forms.Label();
            this.FromInput = new System.Windows.Forms.TextBox();
            this.ToInput = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // FindRide
            // 
            this.FindRide.Location = new System.Drawing.Point(96, 374);
            this.FindRide.Name = "FindRide";
            this.FindRide.Size = new System.Drawing.Size(132, 43);
            this.FindRide.TabIndex = 0;
            this.FindRide.Text = "FIND MY RIDE";
            this.FindRide.UseVisualStyleBackColor = true;
            this.FindRide.Click += new System.EventHandler(this.button1_Click);
            // 
            // RydePool
            // 
            this.RydePool.AutoSize = true;
            this.RydePool.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.RydePool.Location = new System.Drawing.Point(96, 265);
            this.RydePool.Name = "RydePool";
            this.RydePool.Size = new System.Drawing.Size(74, 17);
            this.RydePool.TabIndex = 1;
            this.RydePool.TabStop = true;
            this.RydePool.Text = "Ryde Pool";
            this.RydePool.UseVisualStyleBackColor = true;
            this.RydePool.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // RydeDirect
            // 
            this.RydeDirect.AutoSize = true;
            this.RydeDirect.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.RydeDirect.Location = new System.Drawing.Point(96, 311);
            this.RydeDirect.Name = "RydeDirect";
            this.RydeDirect.Size = new System.Drawing.Size(81, 17);
            this.RydeDirect.TabIndex = 2;
            this.RydeDirect.TabStop = true;
            this.RydeDirect.Text = "Ryde Direct";
            this.RydeDirect.UseVisualStyleBackColor = true;
            this.RydeDirect.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // FromLabel
            // 
            this.FromLabel.AutoSize = true;
            this.FromLabel.Location = new System.Drawing.Point(52, 159);
            this.FromLabel.Name = "FromLabel";
            this.FromLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FromLabel.Size = new System.Drawing.Size(36, 13);
            this.FromLabel.TabIndex = 3;
            this.FromLabel.Text = "From :";
            this.FromLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.FromLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // ToLabel
            // 
            this.ToLabel.AutoSize = true;
            this.ToLabel.Location = new System.Drawing.Point(52, 204);
            this.ToLabel.Name = "ToLabel";
            this.ToLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ToLabel.Size = new System.Drawing.Size(26, 13);
            this.ToLabel.TabIndex = 4;
            this.ToLabel.Text = "To :";
            // 
            // FromInput
            // 
            this.FromInput.Location = new System.Drawing.Point(96, 155);
            this.FromInput.Name = "FromInput";
            this.FromInput.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.FromInput.Size = new System.Drawing.Size(132, 20);
            this.FromInput.TabIndex = 5;
            this.FromInput.TextChanged += new System.EventHandler(this.FromInput_TextChanged);
            // 
            // ToInput
            // 
            this.ToInput.Location = new System.Drawing.Point(96, 201);
            this.ToInput.Name = "ToInput";
            this.ToInput.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ToInput.Size = new System.Drawing.Size(132, 20);
            this.ToInput.TabIndex = 6;
            this.ToInput.TextChanged += new System.EventHandler(this.ToInput_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.Location = new System.Drawing.Point(123, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(66, 50);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(134, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Ryde";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(332, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ToInput);
            this.Controls.Add(this.FromInput);
            this.Controls.Add(this.ToLabel);
            this.Controls.Add(this.FromLabel);
            this.Controls.Add(this.RydeDirect);
            this.Controls.Add(this.RydePool);
            this.Controls.Add(this.FindRide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "HomePage";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "HomePage";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button FindRide;
        private System.Windows.Forms.RadioButton RydePool;
        private System.Windows.Forms.RadioButton RydeDirect;
        private System.Windows.Forms.Label FromLabel;
        private System.Windows.Forms.Label ToLabel;
        public System.Windows.Forms.TextBox FromInput;
        private System.Windows.Forms.TextBox ToInput;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}

