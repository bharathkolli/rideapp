﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*  Vamsee Krishna Bongu C0740053
 *  Bharath Reddy Kolli C0737603
 */

namespace RYDEApp
{
    public partial class HomePage : Form
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FromInput.Focus();
        }

        public void button1_Click(object sender, EventArgs e)
        {
            double baseFare = 2.50;
            double serFee = 1.75;
            double totalPrice, pricePerKm = 0.81;
            string rType = "";
            double MinimumFare = 5.50;
            DateTime currentTime = DateTime.Now;

            if (FromInput.Text == "275 Yorkland Blvd" && ToInput.Text == "CN Tower")
            {
                double distance = 22.9;
                double distanceCharge;

                if (RydePool.Checked == true)
                {
                    rType = "RydePool";
                    distanceCharge = distance * pricePerKm;

                    if ((Convert.ToDateTime("10:00:00 AM") <= currentTime && Convert.ToDateTime("12:00:00 PM") >= currentTime) ||
                       (Convert.ToDateTime("04:00:00 PM") <= currentTime && Convert.ToDateTime("06:00:00 PM") >= currentTime) ||
                       (Convert.ToDateTime("08:00:00 PM") <= currentTime && Convert.ToDateTime("09:00:00 PM") >= currentTime))
                    {
                        distanceCharge = Math.Round((distance * (pricePerKm *1.2)), 2);
                        totalPrice = Math.Round((baseFare + distanceCharge + serFee), 2);
                    }
                    else
                    {
                        totalPrice = Math.Round((baseFare + distanceCharge + serFee), 2);
                    }
                    if (totalPrice < MinimumFare)
                    {
                        totalPrice = MinimumFare;
                    }
                    Console.WriteLine(totalPrice);
                    this.Hide();
                    Confirmed cs = new Confirmed(FromInput.Text, ToInput.Text, baseFare, serFee, distanceCharge, totalPrice, rType);
                    cs.Show();
                }
                else if (RydeDirect.Checked == true)
                {
                    rType = "RydeDirect";
                    distanceCharge = Math.Round((distance * (pricePerKm * 1.15)), 2);
                    baseFare= Math.Round((baseFare * 1.1), 2);



                    if ((Convert.ToDateTime("10:00:00 AM") <= currentTime && Convert.ToDateTime("12:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("04:00:00 PM") <= currentTime && Convert.ToDateTime("06:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("08:00:00 PM") <= currentTime && Convert.ToDateTime("09:00:00 PM") >= currentTime))
                    {
                        distanceCharge = Math.Round((distance * (pricePerKm * 1.2)), 2);
                        //totalPrice = baseFare + distanceCharge + serFee;
                        totalPrice = Math.Round(((baseFare * 1.1) + distanceCharge + serFee), 2);
                    }

                    totalPrice = Math.Round((baseFare + distanceCharge + serFee), 2);

                    if (totalPrice < MinimumFare)
                    {
                        totalPrice = MinimumFare;
                    }
                    Console.WriteLine(totalPrice);
                    this.Hide();
                    Confirmed cs = new Confirmed(FromInput.Text, ToInput.Text, baseFare, serFee, distanceCharge, totalPrice, rType);
                    cs.Show();

                }
                else if (RydePool.Checked == false && RydeDirect.Checked == false)
                {
                    MessageBox.Show("please select the Ryde Type");
                }


            }
            else if (FromInput.Text == "Fairview Mall" && ToInput.Text == "Tim Hortons")
            {
                double distance = 1.2;
                double distanceCharge = distance * pricePerKm;
                if (RydePool.Checked == true)
                {
                    rType = "RydePool";
                    if ((Convert.ToDateTime("10:00:00 AM") <= currentTime && Convert.ToDateTime("12:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("04:00:00 PM") <= currentTime && Convert.ToDateTime("06:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("08:00:00 PM") <= currentTime && Convert.ToDateTime("09:00:00 PM") >= currentTime))
                    {
                        distanceCharge *= 1.2;
                    }

                        totalPrice = Math.Round((baseFare + distanceCharge + serFee), 2);

                    if (totalPrice < MinimumFare)
                    {
                        totalPrice = MinimumFare;
                    }


                    Console.WriteLine(totalPrice);
                    this.Hide();
                    Confirmed cs = new Confirmed(FromInput.Text, ToInput.Text, baseFare, serFee, distanceCharge, totalPrice, rType);
                    cs.Show();
                }
                else if (RydeDirect.Checked == true)
                {
                    rType = "RydeDirect";
                    distanceCharge = distance * (pricePerKm * 1.15);
                    baseFare = baseFare * 1.1;
                    if ((Convert.ToDateTime("10:00:00 AM") <= currentTime && Convert.ToDateTime("12:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("04:00:00 PM") <= currentTime && Convert.ToDateTime("06:00:00 PM") >= currentTime) ||
                        (Convert.ToDateTime("08:00:00 PM") <= currentTime && Convert.ToDateTime("09:00:00 PM") >= currentTime))
                    {
                        distanceCharge *= 1.2;
                    }

                        totalPrice = baseFare + distanceCharge + serFee;
                    if (totalPrice < MinimumFare)
                    {
                        totalPrice = MinimumFare;
                    }


                    Console.WriteLine(totalPrice);
                    this.Hide();
                    Confirmed cs = new Confirmed(FromInput.Text, ToInput.Text, baseFare, serFee, distanceCharge, totalPrice, rType);
                    cs.Show();
                }
                else if (RydePool.Checked == false && RydeDirect.Checked == false)
                {
                    MessageBox.Show("please select the Ryde Type");
                }



            }
            else if (FromInput.Text == "" || ToInput.Text == "")
            {
                MessageBox.Show("Please enter valid From and To Locations:");
            }

            else
                MessageBox.Show("The combination is not valid.");
                
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        

        private void ToInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void FromInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
