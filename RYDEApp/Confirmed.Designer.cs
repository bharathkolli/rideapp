﻿namespace RYDEApp
{
    partial class Confirmed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        HomePage hp = new HomePage();
        private void InitializeComponent()
        {
            this.confirm = new System.Windows.Forms.Label();
            this.PoolType = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.getFrom = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.getTo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.getBooFee = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.getServiceFee = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.getPerKmFee = new System.Windows.Forms.Label();
            this.getTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // confirm
            // 
            this.confirm.AutoSize = true;
            this.confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirm.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.confirm.Location = new System.Drawing.Point(179, 67);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(86, 20);
            this.confirm.TabIndex = 0;
            this.confirm.Text = "Confirmed!";
            this.confirm.Click += new System.EventHandler(this.label1_Click);
            // 
            // PoolType
            // 
            this.PoolType.AutoSize = true;
            this.PoolType.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.PoolType.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PoolType.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PoolType.Location = new System.Drawing.Point(30, 65);
            this.PoolType.Name = "PoolType";
            this.PoolType.Size = new System.Drawing.Size(78, 22);
            this.PoolType.TabIndex = 1;
            this.PoolType.Text = "pooltype";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(31, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "From:";
            // 
            // getFrom
            // 
            this.getFrom.AutoSize = true;
            this.getFrom.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getFrom.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getFrom.Location = new System.Drawing.Point(138, 115);
            this.getFrom.Name = "getFrom";
            this.getFrom.Size = new System.Drawing.Size(104, 17);
            this.getFrom.TabIndex = 3;
            this.getFrom.Text = "from goes here";
            this.getFrom.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(31, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "To:";
            // 
            // getTo
            // 
            this.getTo.AutoSize = true;
            this.getTo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getTo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getTo.Location = new System.Drawing.Point(138, 142);
            this.getTo.Name = "getTo";
            this.getTo.Size = new System.Drawing.Size(70, 17);
            this.getTo.TabIndex = 5;
            this.getTo.Text = "input.Text";
            this.getTo.Click += new System.EventHandler(this.getTo_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(31, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Booking Fee:";
            // 
            // getBooFee
            // 
            this.getBooFee.AutoSize = true;
            this.getBooFee.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getBooFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getBooFee.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getBooFee.Location = new System.Drawing.Point(169, 191);
            this.getBooFee.Name = "getBooFee";
            this.getBooFee.Size = new System.Drawing.Size(39, 17);
            this.getBooFee.TabIndex = 7;
            this.getBooFee.Text = "input";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(31, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "Distance Charge:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(31, 250);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 17);
            this.label10.TabIndex = 10;
            this.label10.Text = "ServiceFee:";
            // 
            // getServiceFee
            // 
            this.getServiceFee.AutoSize = true;
            this.getServiceFee.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getServiceFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getServiceFee.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getServiceFee.Location = new System.Drawing.Point(167, 250);
            this.getServiceFee.Name = "getServiceFee";
            this.getServiceFee.Size = new System.Drawing.Size(39, 17);
            this.getServiceFee.TabIndex = 11;
            this.getServiceFee.Text = "input";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Location = new System.Drawing.Point(31, 347);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 22);
            this.label12.TabIndex = 12;
            this.label12.Text = "Total:";
            // 
            // getPerKmFee
            // 
            this.getPerKmFee.AutoSize = true;
            this.getPerKmFee.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getPerKmFee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getPerKmFee.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getPerKmFee.Location = new System.Drawing.Point(169, 220);
            this.getPerKmFee.Name = "getPerKmFee";
            this.getPerKmFee.Size = new System.Drawing.Size(39, 17);
            this.getPerKmFee.TabIndex = 13;
            this.getPerKmFee.Text = "input";
            // 
            // getTotal
            // 
            this.getTotal.AutoSize = true;
            this.getTotal.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.getTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getTotal.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.getTotal.Location = new System.Drawing.Point(140, 347);
            this.getTotal.Name = "getTotal";
            this.getTotal.Size = new System.Drawing.Size(68, 22);
            this.getTotal.TabIndex = 14;
            this.getTotal.Text = "label13";
            // 
            // Confirmed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(310, 450);
            this.Controls.Add(this.getTotal);
            this.Controls.Add(this.getPerKmFee);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.getServiceFee);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.getBooFee);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.getTo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.getFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PoolType);
            this.Controls.Add(this.confirm);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Name = "Confirmed";
            this.Text = "BookingConfirmed";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label confirm;
        private System.Windows.Forms.Label PoolType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label getFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label getTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label getBooFee;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label getServiceFee;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label getPerKmFee;
        private System.Windows.Forms.Label getTotal;
    }
}